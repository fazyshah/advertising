<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });




    $api->group(['middleware' => 'adminmiddleware'], function(Router $api) {

        $api->get('getuser','App\Http\Controllers\AdminController@getUser');
        $api->get('getadvertisement','App\Http\Controllers\AdminController@getAdvertisement');
        $api->post('addadvertisement','App\Http\Controllers\AdminController@addAdvertisement');
        $api->post('deleteadvertisement','App\Http\Controllers\AdminController@deleteAdvertisement');
        $api->post('updateadvertisement','App\Http\Controllers\AdminController@updateAdvertisement');
        $api->get('getpayment','App\Http\Controllers\AdminController@getpayment');

    });

    $api->group(['middleware' => 'usermiddleware'], function(Router $api) {
        $api->get('useraddview','App\Http\Controllers\AdvertiserController@userAddView');

    });

    $api->group(['middleware' => 'advertisermiddleware'], function(Router $api) {
        $api->post('createadvert','App\Http\Controllers\AdvertiserController@createAdvert');
        $api->get('getalladverts','App\Http\Controllers\AdvertiserController@getAllAdverts');
        $api->post('editadverts','App\Http\Controllers\AdvertiserController@editAdverts');
        $api->get('addviewcount','App\Http\Controllers\AdvertiserController@addViewCount');
        $api->get('addviewno','App\Http\Controllers\AdvertiserController@addViewNo');

    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
