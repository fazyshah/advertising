<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $table='advertisement';

    protected $fillable=['advertisement_name','advertisement_description','advertisement_image','user_id'];

    public function com(){
        return $this->belongsTo('App\User','user_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
