<?php

namespace App\Http\Controllers;

use App\Adds;
use App\Api\V1\Requests\LoginRequest;
use App\Payments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Redirect;

class WebAdminController extends Controller
{
    //
    public function weblogin(){


        $credentials = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password')
        );



        if (Auth::attempt($credentials)) {
            $user_id=User::where('email',Input::get('email'))->pluck('id');
            //$user_id=$user_id->id;
            // validation successful!
            // redirect them to the secure section or whatever
            // return Redirect::to('secure');
            // for now we'll just echo success (even though echoing in a controller is bad)
            Session::put('userid', $user_id);
            return Redirect::to('admin/index');



        } else {

            // validation not successful, send back to form
            return Redirect::to('admin/login');

        }

    }


    public function getUsers()
    {
        $user=User::get();
        return view('admin.tables-user', ['user' => $user]);


    }
    public function getAdds(){
        $ads=Adds::get();
        return view('admin.tables-advertisement', ['adds' => $ads]);
    }




}
