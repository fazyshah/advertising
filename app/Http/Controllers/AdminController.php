<?php

namespace App\Http\Controllers;

use App\Advertisement;
use App\Payments;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Adds;
use App\AddsView;


class AdminController extends Controller
{
    public function admin(){
        return response()->json(['success'=>'true'],200);
    }

    public function getUser(){
        $user_id = Auth::user()->id;
        $user = User::where('user_id',$user_id)->get();

        return response()->json(['success'=>true,'user'=>$user],200);
    }



    public function getAdvertisement(){
        $user_id = Auth::user()->id;
        $advertisement = Advertisement::where('user_id',$user_id)->get();

        return response()->json(['success'=>true,'advertisement'=>$advertisement],200);
    }


    public function deleteAdvertisement($id){
        Advertisement::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=> 'Advertisement Deleted successfully'],200);

    }
    public function updateAdvertisement(Request $request ){

        if ($request->has('id')&&$request->has('description')
            &&$request->hasFile('image')&&$request->has('expires_at'))
        {
            $advertisement_id=Adds::where('id',$request->id)->get();
            $file = $request->file('image');

            $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

            $request->file('image')->move("uploads", $name);

            if(!$advertisement_id->isEmpty()){
                Adds::where('id',$request['id'])->update(['description'=>$request->description,
                    'image'=>$name,'expires_at'=>$request->expires_at]);

                return response()->json(['success'=>true,'message'=> 'Advertisement Updated successfully'],200);
            }else{
                return response()->json(['success'=>false,'message'=> 'Invalid Advertisement'],200);

            }


        }
        else {
            return response()->json(['success' => false, 'message' => 'Unable To Update Company'], 200);
        }

    }


    public function getpayment()
    {
        $payments=Payments::get();
        return view('admin.tables-payments', ['payments' => $payments]);
    }

}
