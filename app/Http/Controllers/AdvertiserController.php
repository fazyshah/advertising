<?php

namespace App\Http\Controllers;

use App\Adds;
use App\AddsView;
use Auth;
use Illuminate\Http\Request;

class AdvertiserController extends Controller
{
    //
    public function createadvert(Request $request)
    {
        if ($request->has('description') && $request->hasFile('image')) {
            $advertisement = new Adds();
            $file = $request->file('image');

            $name = "/uploads/" . time() . '.' . $file->getClientOriginalExtension();

            $request->file('image')->move("uploads", $name);
            $advertisement->description = $request->description;
            $advertisement->image = $name;
            $advertisement->date_posted=date("Y-m-d H:i:s");
            $advertisement->expires_at = $request->expires_at;
            $user_id = Auth::user()->id;
            $advertisement->created_by = $user_id;

            $advertisement->save();
            return response()->json(['success' => true, 'message' => 'Advertisement Added'], 200);
        } else {
            return response()->json(['success' => false, 'message' => 'Please Fill In The Required Fields'], 200);
        }
    }

    public function getAllAdverts()
    {
        $user_id = Auth::user()->id;
        $adverts = Adds::where('created_by', $user_id)->with('user')->get();
        return response()->json(['success' => true, 'ads' => $adverts], 200);
    }

    public function editAdverts(Request $request){

        if ($request->has('id')&&$request->has('description')
            &&$request->hasFile('image')&&$request->has('expires_at'))
        {
            $advertisement_id=Adds::where('id',$request->id)->get();
            $file = $request->file('image');

            $name = "/uploads/".time() . '.' . $file->getClientOriginalExtension();

            $request->file('image')->move("uploads", $name);

            if(!$advertisement_id->isEmpty()){
                Adds::where('id',$request['id'])->update(['description'=>$request->description,
                    'image'=>$name,'expires_at'=>$request->expires_at]);

                return response()->json(['success'=>true,'message'=> 'Advertisement Updated successfully'],200);
            }else{
                return response()->json(['success'=>false,'message'=> 'Invalid Advertisement'],200);

            }


        }
        else {
            return response()->json(['success' => false, 'message' => 'Unable To Update Company'], 200);
        }

    }


    public function deleteAdverts($id){
        Adds::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=> 'Advertisement Deleted successfully'],200);

    }

    public function addViewCount(Request $request){
        $addview = new AddsView();
        $addview->adds_id = $request->adds_id;
        $user_id = Auth::user()->id;
        $addview->save();

        return response()->json(['success'=>true,'message'=> 'Record added successfully'],200);

    }

    public function addViewNo($id){
        AddsView::where('id',$id)->count();
        return response()->json(['success'=>true,'message'=> 'Adds view  '],200);

    }
    public function userAddView(Adds $adds){

        return response()->json(['success'=>true, 'adds' => $adds], 200);

    }



}
