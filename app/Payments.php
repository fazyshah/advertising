<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table= 'payment';

    protected $fillable=['user_id','amount'];

    public function adview(){
        return $this->belongsTo('App\User','user_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
