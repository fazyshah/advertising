<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddsView extends Model
{
    protected $table= 'addview';

    protected $fillable=['adds_id','user_id'];

    public function adview(){
        return $this->belongsTo('App\User','user_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
    public function advie(){
        return $this->belongsTo('App\Adds','adds_id','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
