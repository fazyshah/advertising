<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adds extends Model
{
    protected $table= 'adds';

    protected $fillable=['description','created_by','image','date_posted','expires_at'];

    public function user(){
        return $this->belongsTo('App\User','created_by','id');//defined role_id as fkey in user& id is pkey in Role
    }
}
