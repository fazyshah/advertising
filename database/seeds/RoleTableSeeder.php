<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Role::create(['role'=>'Admin']);
        Role::create(['role'=>'User']);
        Role::create(['role'=>'Advertiser']);

    }
}
